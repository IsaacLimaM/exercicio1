const express = require('express');
const bodyParser = require ('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port, ()=>{
    console.log("Projeto rodando na porta: " + port);
});

app.get('/clientes', (req, res)=>{
    res.send("{message:cliente encontrado");
});

app.get('/clientes/appex', (req, res)=>{
    let source= req.query;
    let ret = "nome: " + source.nome + " " + source.sobrenome;
    res.send("{message:"+ret+"}");
});

app.post('/clientes', (req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Access: "+ headers_);
    let ret = "Dados enviados: Nome: " + dados.nome;
    ret+=" Sobrenome: "+ dados.sobrenome;
    res.send("{message: "+ret+"}");
});