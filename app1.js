const express = require('express');
const bodyParser = require ('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port, ()=>{
    console.log("Projeto rodando na porta: " + port);
});

app.get('/funcionarios', (req, res)=>{
    res.send("{message:funcionario encontrado");
});

app.get('/funcionarios/appex', (req, res)=>{
    let source= req.query;
    let ret = "nome: " + source.nome + " " + source.sobrenome;
    res.send("{message:"+ret+"}");
});

app.delete('/funcionarios/delete/:valor',(req, res)=>{
    let dados = req.params.valor
    let headers_ = req.headers["access"]

    if(headers_ == "123456"){
        res.send("{Mensagem: Valor: " + dados + "}");
    }else{
        res.send("Inválido!")
    }
});

app.put('/funcionarios',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"]

    let ret = "Dados do Funcionario: Nome do Funcionario: " + dados.nomeF;
    ret+=" Filial: " + dados.Filial;
    
    if(headers_ == "123456"){
        res.send("{Mensagem: "+ret+"}");
    }else{
        res.send("Inválido! ")
    }
});